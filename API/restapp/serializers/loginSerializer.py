
from django.contrib.auth.hashers import check_password
from rest_framework import serializers

from restapp.models import User

from .serializers import UserSerializer
class LoginSerializer(serializers.Serializer):
    """
    This serializer defines two fields for authentication:
      * username
      * password.
    It will try to authenticate the user with when validated.
    """
    username = serializers.CharField(
        label="Username",
        write_only=True
    )
    password = serializers.CharField(
        label="Password",
        # This will be used when the DRF browsable API is enabled
        style={'input_type': 'password'},
        trim_whitespace=False,
        write_only=True
    )
    def validate(self, attrs):
        # Take username and password from request
        username = attrs.get('username')
        password = attrs.get('password')
        if username and password:
            # Try to authenticate the user using Django auth framework.
            user = MyBackend.authenticate(self,request=self.context.get('request'),
                                username=username, password=password)
            if not user:
                # If we don't have a regular user, raise a ValidationError
                msg = 'Access denied: wrong username or password.'
                raise serializers.ValidationError(msg, code='authorization')
        else:
            msg = 'Both "username" and "password" are required.'
            raise serializers.ValidationError(msg, code='authorization')
        # We have a valid user, put it in the serializer's validated_data.
        # It will be used in the view.
        attrs['user'] = user
        return attrs

from django.contrib.auth.backends import BaseBackend, ModelBackend


class MyBackend(BaseBackend):
    def authenticate(self, request, username=None, password=None):
        try:
            user= User.objects.get(username=username)
        except User.DoesNotExist:
            return False
        #if check_password(password, user.password):
        if check_password(password, user.password):
        #set is_active to true if false inside user model
            return user
        else:
            msg= "false password"
            raise serializers.ValidationError(msg, code='authorization')
             
        #user=UserSerializer(user)

